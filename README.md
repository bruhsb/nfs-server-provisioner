# nfs-provisioner

[![Docker Repository on Quay](https://quay.io/repository/kubernetes_incubator/nfs-provisioner/status "Docker Repository on Quay")](https://quay.io/repository/kubernetes_incubator/nfs-provisioner)
```
quay.io/kubernetes_incubator/nfs-provisioner:v1.0.8
```

nfs-provisioner is an out-of-tree dynamic provisioner for Kubernetes +1.4. You can use it to quickly & easily deploy shared storage that works almost anywhere.

It works just like in-tree dynamic provisioners: a `StorageClass` object can specify an instance of nfs-provisioner to be its `provisioner` like it specifies in-tree provisioners such as GCE. Then, the instance of nfs-provisioner will watch for `PersistentVolumeClaims` that ask for the `StorageClass` and automatically create NFS-backed `PersistentVolumes` for them.

For more information on how dynamic provisioning works, see [the official docs](http://kubernetes.io/docs/user-guide/persistent-volumes/) or [this blog post](http://blog.kubernetes.io/2016/10/dynamic-provisioning-and-storage-in-kubernetes.html).

## Quickstart
Choose some volume for your nfs-provisioner instance to store its state & data in and mount the volume at `/export` in `statefulset.yaml`. Note that the volume must have a [supported file system](https://github.com/nfs-ganesha/nfs-ganesha/wiki/Fsalsupport#vfs) on it: any local filesystem on Linux is supported & NFS is not supported.
```yaml
...
  volumeMounts:
  - mountPath: /export
    name: data
...
  volumeClaimTemplates:
  - metadata:
      name: data
    spec:
      accessModes:
      - ReadWriteOnce
      resources:
        requests:
          storage: 100Gi
...
```

Choose a `provisioner` name for a `StorageClass` to specify and set it in `statefulset.yaml`
```yaml
...
  args:
    - -provisioner=cluster.local/storage-nfs-server-provisioner
...
```

Create the statefulset.
```console
$ kubectl create -f statefulset.yaml
service "storage-nfs-server-provisionerr" created
statefulset "storage-nfs-server-provisioner" created
```

Create a `StorageClass` named "nfs" with `provisioner: cluster.local/storage-nfs-server-provisioner`.
```console
$ kubectl create -f storageclass.yaml
storageclass "nfs" created
```

To authorize nfs-provisioner on a Kubernetes cluster, create a `ClusterRoleBinding` for RBAC

```console
$ kubectl apply -f rolebinding.yaml
clusterrolebinding "storage-nfs-server-provisioner" created

$ kubectl apply -f serviceaccount.yaml
serviceaccount "storage-nfs-server-provisioner" created
```

Create a `PersistentVolumeClaim` with annotation `volume.beta.kubernetes.io/storage-class: "nfs"`
```console
$ kubectl create -f pvclaim.yaml
persistentvolumeclaim "volume-claim" created
```

A `PersistentVolume` is provisioned for the `PersistentVolumeClaim`. Now the claim can be consumed by some pod(s) and the backing NFS storage read from or written to.
```console
$ kubectl get pv
NAME                                       CAPACITY   ACCESSMODES   RECLAIMPOLICY   STATUS      CLAIM         REASON    AGE
pvc-dce84888-7a9d-11e6-b1ee-5254001e0c1b   40Gi        RWX           Delete          Bound       default/nfs             23s
```

Deleting the `PersistentVolumeClaim` will cause the provisioner to delete the `PersistentVolume` and its data.

Deleting the provisioner deployment will cause any outstanding `PersistentVolumes` to become unusable for as long as the provisioner is gone.

## References: Kubernetes Incubator

This is a fork of [Kubernetes Incubator project](https://github.com/kubernetes/community/blob/master/incubator.md). The project was established 2016-11-15. The incubator team for the project is:

- Sponsor: Clayton (@smarterclayton)
- Champion: Brad (@childsb)
- SIG: sig-storage
